---
title: "Contact"
date: 2019-03-01T13:38:47+01:00
---

# How to contact me

Before contacting, try these:

- For DIMO questions, try [dimo.zone](https://dimo.zone)

You can find me on these networks:

- email: `AC@dimo dot zone` 
- github: [@ajchatham](https://github.com/ajchatham)
- twitter: [@ajchatham](https://twitter.com/ajchatham)
- keybase: [@ajchatham](https://keybase.io/ajchatham)

**Please Note:** These days I receive vastly more messages than I can reasonably respond to. If I don't respond, please don't take it personally. I am more likely to respond if you get an introduction from someone I know (think of it like a _proof-of-work_).
